import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Lottery {
    public static void main(String[] args) {
        Scanner lukija = new Scanner(System.in);
        Random random = new Random();
        ArrayList<Integer> rivi = new ArrayList<>();
        
        System.out.println("lotto, euro, viking, keno");
        String veikkaus = lukija.nextLine();
        
        switch (veikkaus) {
            case "lotto":
                arvoLotto(rivi, random);
                break;
            case "euro":
                arvoEurojackpot(rivi, random);
                break;
            case "viking":
                arvoVikinglotto(rivi, random);
                break;
            case "keno":
                arvoKeno(rivi, random, lukija);
                break;
            default:
                break;
        }
    }
    
    private static void arvoLotto (ArrayList<Integer> rivi, Random random) {
        while (rivi.size() < 7) {
                int number = random.nextInt(40) + 1;
                if (!rivi.contains(number)) {
                    rivi.add(number);
                }
            }
            Collections.sort(rivi);
            System.out.println("Lotto rivi: " +rivi);
            System.out.println("Plusnumero: " +(random.nextInt(30) + 1));
    }
    
    private static void arvoEurojackpot (ArrayList<Integer> rivi, Random random) {
        while (rivi.size() < 5) {
                int number = random.nextInt(50) + 1;
                if (!rivi.contains(number)) {
                    rivi.add(number);
                }
            }
            Collections.sort(rivi);
            System.out.println("Euro rivi: " +rivi);
            rivi.clear();
            while (rivi.size() < 2) {
                int number = random.nextInt(10) + 1;
                if (!rivi.contains(number)) {
                    rivi.add(number);
                }
            }
            Collections.sort(rivi);
            System.out.println("Tähtinumerot: " +rivi);
    }
    
    private static void arvoVikinglotto (ArrayList<Integer> rivi, Random random) {
        while (rivi.size() < 6) {
                int number = random.nextInt(48) + 1;
                if (!rivi.contains(number)) {
                    rivi.add(number);
                }
            }
            Collections.sort(rivi);
            System.out.println("Viking rivi: " +rivi);
            System.out.println("Vikingnumero: " +(random.nextInt(8) + 1));
            System.out.println("Plusnumero: " +(random.nextInt(30) + 1));
    }
    
    private static void arvoKeno (ArrayList<Integer> rivi, Random random, Scanner lukija) {
        System.out.println("Valitse taso[2-10]: ");
            int taso = Integer.valueOf(lukija.nextLine());
            
            while (rivi.size() < taso) {
                int number = random.nextInt(70) + 1;
                if (!rivi.contains(number)) {
                    rivi.add(number);
                }
            }
            Collections.sort(rivi);
            System.out.println("Keno rivi: " +rivi);
    }
}